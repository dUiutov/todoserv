var Rx = require("rxjs/Rx.js");
var ObjectID = require("mongodb").ObjectID;

const taskCollection = "Tasks";
const userCollection = "Users";
client = require("mongodb").MongoClient;
hasher = require("object-hash");

function MongoDBforTasks(url, dbName) {
    this.url = url + '/' + dbName;
}

MongoDBforTasks.prototype.getUserID = function (login, password) {
    return client.connect(this.url).then(function (db) {
        return db.collection(userCollection).find({
            login: login,
            password: hasher(login + password)
        }).toArray().then(function (val) {
            db.close();
            if (val.length) {
                return val[0].uid;
            } else {
                return Promise.reject(false);
            }
        });
    });
};

MongoDBforTasks.prototype.findUserFromID = function (uid) {
    return client.connect(this.url).then(function (db) {
        return db.collection(userCollection).find({uid: parseFloat(uid)}).toArray().then(function (val) {
            db.close();
            return val.length ? val[0] : Promise.reject(false);
        })
    })
};

MongoDBforTasks.prototype.addRecord = function (obj, uid) {
    obj.uid = uid;
    return client.connect(this.url).then(function (db) {
        return db.collection(taskCollection).insertOne(obj).then(function (obj) {
            db.close();
            return JSON.stringify(obj.ops[0]._id);
        });
    });
};

MongoDBforTasks.prototype.deleteRecord = function (term) {
    return client.connect(this.url).then(function (db) {
        return db.collection(taskCollection).deleteOne({_id: new ObjectID(term._id)}).then(function () {
            return true;
        })
    });
};

MongoDBforTasks.prototype.updateRecord = function (term, newRecord) {
    return client.connect(this.url).then(function (db) {
        return db.collection(taskCollection).updateOne({_id: new ObjectID(term._id)}, {
                $set: {
                    description: newRecord.description,
                    createDate: newRecord.createDate,
                    status_: newRecord.status_,
                    plannedStartDate_: newRecord.plannedStartDate_,
                    startDate: newRecord.startDate,
                    plannedEstimate_: newRecord.plannedEstimate_,
                    spendTime_: newRecord.spendTime_,
                    lastStart: newRecord.lastStart
                }
            },
            {upsert: false}).then(function (val) {
            db.close();
            return true;
        })
    });
};

MongoDBforTasks.prototype.getRecords = function (term) {
    return client.connect(this.url).then(function (db) {
        return db.collection(taskCollection).find(term).toArray().then(function (val) {
            val.forEach(function (item) {
                for (var prop in item) {
                    if (!item[prop] && prop != 'status_') {
                        delete item[prop];
                    }
                }
            });
            db.close();
            return val;
        });
    });
};

MongoDBforTasks.prototype.clearDB = function () {
    client.connect(this.url, function (err, db) {
        if (err) {
            console.log(err);
            return;
        }
        db.collection(taskCollection).deleteMany({}, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('Delete all records');
            }
            db.close();
        })
    })
};

MongoDBforTasks.prototype.registration = function (login, password) {
    return client.connect(this.url).then(function (db) {
        return db.collection(userCollection).find({login: login}).toArray().then(function (val) {
            if (val.length) {
                db.close();
                return Promise.reject(false);
            }
            var pass = hasher(login + password);
            var uid;
            (function createUid() {
                uid = Math.random();
                return db.collection(userCollection).find({uid: uid}).toArray().then(function (val) {
                    if (val.length) {
                        return createUid();
                    } else {
                        return true;
                    }
                });
            })().then(function () {
                return db.collection(userCollection).insertOne({
                    login: login,
                    password: pass,
                    uid: uid
                }).then(function () {
                    db.close();
                    return uid;
                });
            });
        });
    });
};

module.exports = MongoDBforTasks;