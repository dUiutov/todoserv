var Mongo = require("./app/MongoDBforTasks");
var app = require('express')();

var bodyParser = require("body-parser").json();
var cookieParser = require("cookie-parser");

const dbURL = "mongodb://localhost:27017";

var db = new Mongo(dbURL, "TaskDB");


var options = {root: __dirname + '/..'};

app.use(bodyParser);
app.use(cookieParser());

app.use(function (request, response, next) {
    var time = new Date();
    request.time = time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds() + ' ' + time.getDate() + '.' + (time.getMonth() + 1) + '.' + time.getFullYear() + ' - ';
    console.log(request.time + request.method + ' - '+ request.path);
    next();
});

app.get('/authorisation', function (request, response) {
    db.findUserFromID(request.cookies.uid).then(function () {
        response.cookie('uid', request.cookies.uid, {httpOnly: true}).end();
    }, function () {
        response.status(401).end();
    });
});

app.post('/login', function (request, response) {
    db.getUserID(request.body.login, request.body.password).then(function (val) {
        console.log('Login ' + val);
        response.cookie('uid', val, {httpOnly: true}).status(200).end();
    }, function () {
        console.log('Login failed');
        response.status(401).end();
    })
});

app.post('/registration', function (request, response) {
    db.registration(request.body.login, request.body.password).then(function (val) {
        console.log('Registration - OK');
        response.cookie('uid', val, {httpOnly: true, secure: true}).status(200).end();
    }, function () {
        console.log('Registratyion failed');
        response.status(401).end();
    })
});

//***************** Dependencies
function resolveDependencies(request, response) {
    response.sendFile('/todoapp' + request.path, options);
}

app.get('/node_modules/*', resolveDependencies);

app.get('/app/*', resolveDependencies);

app.get('/systemjs.config.js', resolveDependencies);

app.get('/styles.css', resolveDependencies);

app.get('/favicon.ico', resolveDependencies);
//*****************

app.use('/*', function (request, response, next) { console.log(request.cookies.uid);
    console.log('Authorization check...');
    if (request.cookies.uid) {
        db.findUserFromID(request.cookies.uid).then(function () {
            console.log('Valid autorisation UID - ' + request.cookies.uid);
            next();
        }, function () {
            console.log('Invalid UID');
            response.status(401).sendFile('/todoapp/index.html', options);
        });
    } else {
        console.log('Invalid autorisation Not UID');
        response.status(401).sendFile('/todoapp/index.html', options);
    }
});

app.get('/logout', function (request, response) {
    response.cookie('uid', '').end();
    console.log('Logout');
});

app.get('/task', function (request, response) {
    db.getRecords({uid: request.cookies.uid}).then(function (val) {
        if (val) {
            response.send(val).end();
        } else {
            response.end();
        }
    }, function () {
        response.end();
    });
});

app.get('/clearTasks', function () {
    db.clearDB();
    console.log('Collection cleared');
});

app.post('/task', function (request, response) {
    if (Array.isArray(request.body)) {
        request.body.forEach(function (item) {
            db.updateRecord({title: item.title, createDate: item.createDate}, item);
        })
    } else {
        db.addRecord(request.body, request.cookies.uid).then(function (obj) {
            console.log('Add record');
            response.status(200).end(obj);
        }, function (err) {
            console.log(err);
            response.status(500).end();
        });
    }
});

app.post('/updateTask', function (request, response) {
    db.updateRecord({_id: request.body._id}, request.body).then(function () {
        response.status(200).end();
        console.log('Task updated');
    }, function (err) {
        console.log(err);
        response.status(500).end();
    });
});

app.post('/deleteTask', function (request, response) {
    db.deleteRecord(request.body).then(function () {
        response.status(200).end();
        console.log('Task deleted');
    }, function (err) {
        console.log(err);
        response.status(500).end();
    })
});

// app.get('/list', function (request, response) {
//     response.sendFile('/todoapp/index.html', options);
// });

app.get('/*', function (request, response) {
    response.sendFile('/todoapp/index.html', options);
});

app.listen(3000);